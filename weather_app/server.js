const express = require('express');
const bodyParser = require('body-parser');
const request = require('request');
const app = express()


app.use(bodyParser.urlencoded({ extended: true }));	  //support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.json());				

app.use(express.static('public'));
app.set('view engine', 'ejs')		
const apiKey = '6008410cb916eb1ac99be33151de0e8a';

app.get('/', function (req, res) {
  res.render('new', {weather: null, error: null,city_name:null,temperature:null});
})

app.get('/user', function (req, res) {
  res.render('user', {weather: null, error: null,city_name:null,temperature:null});
})
//300538
app.post('/', function (req, res) {
   city = req.body.city;
  let url = `http://api.openweathermap.org/data/2.5/weather?q=${city}&units=metric&appid=${apiKey}`

  request(url, function (err, response, body) {
    if(err){
      res.render('new', {weather: null, error: 'Error, please try again',city_name:null,temperature:null});
    } else {
      let weather = JSON.parse(body)
      if(weather.main == undefined){
        res.render('new', {weather: null, error: 'Error, please try again', city_name:null,temperature:null});
      } else {
        let weatherText = `It's ${weather.main.temp} degrees in ${weather.name}!`;
        res.render('new', {weather: weatherText, error: null,city_name:city, temperature :weather.main.temp });
      }
    }
  });
})

app.listen(8080, function () {
  console.log('server started on port 8080!')
})
